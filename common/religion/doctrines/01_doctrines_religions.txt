﻿hostility_group = {
	group = "not_creatable"
	abrahamic_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 2
			hostility_same_family = 3
			hostility_others = 3
		}
	}
	pagan_hostility_doctrine = {
		visible = no
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 2
			hostility_others = 3
		}
	}
	eastern_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 1
			hostility_others = 2
		}
	}
	cannorian_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 1	#astray, maybe turn it righteous? maybe thats for regent court
			hostility_same_family = 1
			hostility_others = 1	#might need to change these but yeah
		}
	}
	sun_cult_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 2
			hostility_same_family = 2
			hostility_others = 2
		}
	}
}
